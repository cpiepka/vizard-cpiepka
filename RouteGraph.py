﻿import vizshape
import ModelCreater

class RoomNode():
	def __init__(self, position, id, connectionNodes = []):
		self.id = id
		self.position = position
		self.connectionNodes = connectionNodes
		self.model = None
	
	
class ConnectionNode():
	def __init__(self, position, id):
		self.id = id
		self.position = position
		self.model = None
		
	def __eq__(self, other):
		if isinstance(other, ConnectionNode):
			return self.id == other.id
		return False
		
	def __hash__(self):
		return hash(self.id)
		
		
class RouteGraph():
	def __init__(self, roomNodes):
		self.roomNodes = roomNodes
		self.connectionNodes = []
		self.edges = []
		self.drawGraph(roomNodes)
		
	def drawGraph(self, roomNodes):
		connectionNodes = set()
		for r in roomNodes:
			r.model = self.createNodeModel(r.position)
			
			for c in r.connectionNodes:
				connectionNodes.add(c)
				self.edges.append(ModelCreater.CreateLine(r.position, c.position))
		self.connectionNodes = connectionNodes
				
		for c in self.connectionNodes:
			c.model = self.createNodeModel(c.position)
		
		
	def createNodeModel(self, position):
		node = vizshape.addSphere(radius=0.5)
		node.setPosition(position)
		node.alpha(0.7)
		return node
	
