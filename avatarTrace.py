﻿import DesignUtils
import CameraUtils
import AvatarUtils
import ModelUtils
import viz
import vizact

gulbenkian = DesignUtils.load_gulbenkian_no_roof()

viz.MainWindow.fov(60)
viz.setMultiSample(4)
viz.clearcolor(viz.SKYBLUE)
viz.go()

CameraUtils.SetFTNCam(gulbenkian)

avatars = AvatarUtils.CreateAvatars('dsim-json/Gulbenkian/IsovistNarrativeRoutes2.JSON', 1.0, 4.59999275208, 'vcc_male.cfg')
ModelUtils.toggleIsovists(avatars)

ModelUtils.AddLineLayers([viz.MainWindow], avatars, 1,0,0, 0.8)
vizact.ontimer(0, ModelUtils.DrawPathToWindow, avatars)