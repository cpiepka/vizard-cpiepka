﻿import json

def openFile(file):
	global jsonData
	if isinstance(file, str):
		return json.loads(file)
	else:	
		from pprint import pprint
		with open(file) as data_file:
			return json.load(data_file)
	
def getObjectData(file):
	jsonData = openFile(file)
	
	retVal =[]
	for data in jsonData['objects']:
		parsedData = {}
		parsedData['id'] = jsonData['objects'][data]['guid']
		parsedData['desc'] = jsonData['objects'][data]['description']
		parsedData['ds_class'] = jsonData['objects'][data]['ds_class_type']
		parsedData['ifc_class'] = jsonData['objects'][data]['ifc_class_type']
		parsedData['name'] = jsonData['objects'][data]['name']
		
		if not jsonData['objects'][data]['representations'] is None:
		
			parsedPolygonData = {}

			parsedPolygonData['elevation'] = float(jsonData['objects'][data]['representations']['Polygon_Footprint']['data']['Elevation'])
			parsedPolygonData['height'] = float(jsonData['objects'][data]['representations']['Polygon_Footprint']['data']['Height'])
			
			parsedContour = []
			for contour in 	jsonData['objects'][data]['representations']['Polygon_Footprint']['data']['Polygon_Data']['contour_list']:
				contourList = []
				for point in contour['point_list']:
					parsedPoint = (float(point['x']),0, float(point['y']))
					contourList.append(parsedPoint)
				parsedContour.append(contourList)
			
			parsedPolygonData['contour'] = parsedContour
			
			parsedHoles = []
			if not jsonData['objects'][data]['representations']['Polygon_Footprint']['data']['Polygon_Data']['hole_list'] is None:
				for hole in jsonData['objects'][data]['representations']['Polygon_Footprint']['data']['Polygon_Data']['hole_list']:
					holeList = []
					for point in hole['point_list']:
						parsedPoint = (float(point['x']),0, float(point['y']))
						holeList.append(parsedPoint)
					parsedHoles.append(holeList)
	
			parsedPolygonData['holes'] = parsedHoles
				
			parsedData['polygonData'] = parsedPolygonData
			
			parsedBoundingSphereData = {}
			
			parsedBoundingSphereData['radius'] = jsonData['objects'][data]['representations']['Bounding_Sphere_Rep']['data']['Sphere_Data']['radius']
			
			sphereCenter_X = float(jsonData['objects'][data]['representations']['Bounding_Sphere_Rep']['data']['Sphere_Data']['centroid']['x'])
			sphereCenter_Y = float(jsonData['objects'][data]['representations']['Bounding_Sphere_Rep']['data']['Sphere_Data']['centroid']['z'])
			sphereCenter_Z = float(jsonData['objects'][data]['representations']['Bounding_Sphere_Rep']['data']['Sphere_Data']['centroid']['y'])
			
			parsedBoundingSphereData['centroid'] = (sphereCenter_X, sphereCenter_Y, sphereCenter_Z)
			
			parsedData['boundingSphere'] = parsedBoundingSphereData
			
			

		retVal.append(parsedData)
	return retVal
	

def setEventData(jsonData, id, eventType):
		retVal = []
		try:
			retVal = jsonData[id]["events"][eventType]
		except KeyError:
			print('Key' + ' ' + str(eventType) + ' not found.')
			return retVal
		return retVal	

def setIsovistData(jsonData, id):
	retVal = []
	unparsed = jsonData[id]["events"]["isovists"]
	contours = []
	holes = []
	for u in unparsed:
		parsedData = {}
		unparsedContour = u["isovist_geometry"]["contours"]
		unparsedHoles = u["isovist_geometry"]["holes"]
		
		for c in unparsedContour:
			parsedContour = []
			for point in c:
				parsedContour.append([point[0], point[1], point[2]])
			contours.append(parsedContour)
		
		if not unparsedHoles is None:
			for h in unparsedHoles:
				parsedHole = []
				for point in h:
					parsedHole.append([point[0], point[1], point[2]])
				holes.append(parsedHole)
		parsedData["contours"] = contours
		parsedData["holes"] = holes
	
		parsedData["eventId"] = u["event_index"]
		parsedData["trigger"] = [u["triggerValue"][0], u["triggerValue"][1], u["triggerValue"][2]]
		
		visibleProducts = []
		for p in u["visible_products"]:
			visibleProducts.append(p)
		parsedData["visibleProducts"] = visibleProducts
		
		retVal.append(parsedData)
	return retVal
	

def getAvatarsData(file):
	jsonData = openFile(file)
	retVal = []
	i = 0
	dataLength = len(jsonData)
	while i < dataLength:
		avatarData = {}
		avatarData["id"] = i
		avatarData["geometric_path"] = jsonData[i]["geometric_path"]
		avatarData["isovists"] = setIsovistData(jsonData, i)
		avatarData["narrative"] = setEventData(jsonData, i, "narrative")
		avatarData["entering_topological_product"] = setEventData(jsonData, i, "entering_topological_product")
		avatarData["visible_at_entrance"] = setEventData(jsonData, i, "visible_at_entrance")
		avatarData["UserAsksForDirections"] = setEventData(jsonData, i, "UserAsksForDirections")
		avatarData["UserIsFrustrated"] = setEventData(jsonData, i, "UserIsFrustrated")
		avatarData["UserLooksAround"] = setEventData(jsonData, i, "UserLooksAround")
		avatarData["UserWaits"] = setEventData(jsonData, i, "UserWaits")
		retVal.append(avatarData)
		i += 1
	return retVal
	
def getArtefactData(file, artefactType):
	unparsedData = openFile(file)[artefactType]
	retVal = []
	for u in unparsedData:
		parsedData = {}
		parsedData["guid"] = u["guid"]
		parsedContour = []
		parsedHoles = []
		for c in u["representation"]["footprint"]["contours"]:
			contour = []
			for point in c:
				contour.append([point[0], point[1], point[2]])
			parsedContour.append(contour)
		parsedData["contours"] = parsedContour
		
		if not u["representation"]["footprint"]["holes"] is None:
			for h in u["representation"]["footprint"]["holes"]:
				holes = []
				for point in h:
					holes.append([point[0], point[1], point[2]])
				parsedHoles.append(holes)
			parsedData["holes"] = parsedHoles
		else:
			parsedData["holes"] = []
		
		parsedData["height"] = u["representation"]["height"]
		parsedData["elevation"] = u["representation"]["elevation"]
		
		retVal.append(parsedData)
	return retVal 


