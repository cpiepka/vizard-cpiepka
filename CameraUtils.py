﻿import viz
import vizcam
import vizinfo

#avatar_link = viz.link(viz.NullLinkable, viz.NullLinkable)
camLinks = {}
linkedAvatarId = -1
#info panel for the narratives
info = vizinfo.InfoPanel("")
info.visible(viz.OFF)

#info panel for the userstates
info2 = vizinfo.InfoPanel("", viz.ALIGN_CENTER_BOTTOM, icon=False)
info2.visible(viz.OFF)

def linkCamBehind(avatar, window):
    #global avatar_link
    global avatarScale
    global linkedAvatarId
    if window is viz.MainWindow and linkedAvatarId != avatar.id:
        linkedAvatarId = avatar.id
        info.visible(viz.OFF)
        info2.visible(viz.OFF)
    #avatar_link.disable()
    if camLinks.has_key(window.getView().id):
        camLinks[window.getView().id].remove()
    avatar_link = viz.link(avatar.vizAvatar, window.getView())
    avatarScale = avatar.vizAvatar.getScale()
    avatar_link.preTrans([0*avatarScale[0],2*avatarScale[1],-3*avatarScale[2]])
    avatar_link.preEuler([0,10,0])
    camLinks[window.getView().id] = avatar_link
    
def linkCamTopDown(avatar, window):
    global avatar_link
    global avatarScale
    global linkedAvatarId
    if window is viz.MainWindow and linkedAvatarId != avatar.id:
        linkedAvatarId = avatar.id
        info.visible(viz.OFF)
        info2.visible(viz.OFF)
    #avatar_link.disable()
    if camLinks.has_key(window.getView().id):
        camLinks[window.getView().id].remove()
    avatar_link = viz.link(avatar.vizAvatar, window.getView())
    avatarScale = avatar.vizAvatar.getScale()
    avatar_link.preTrans([0*avatarScale[0],30*avatarScale[1],0*avatarScale[2]])
    avatar_link.preEuler([0,90,0])
    camLinks[window.getView().id] = avatar_link
    
def linkCamFront(avatar, window):
    global avatar_link
    global avatarScale
    global linkedAvatarId
    if window is viz.MainWindow and linkedAvatarId != avatar.id:
        linkedAvatarId = avatar.id
        info.visible(viz.OFF)
        info2.visible(viz.OFF)
    #avatar_link.disable()
    if camLinks.has_key(window.getView().id):
        camLinks[window.getView().id].remove()
    avatar_link = viz.link(avatar.vizAvatar, window.getView())
    avatarScale = avatar.vizAvatar.getScale()
    avatar_link.preTrans([0*avatarScale[0],2*avatarScale[1],3*avatarScale[2]])
    avatar_link.preEuler([180,10,0])
    camLinks[window.getView().id] = avatar_link

def zoomCam(zoomIn, window):
    if not zoomIn:
        link_preTrans = camLinks[window.getView().id].preTrans([0*avatarScale[0],0*avatarScale[1],-0.25*avatarScale[2]])
    else:
        link_preTrans = camLinks[window.getView().id].preTrans([0*avatarScale[0],0*avatarScale[1],0.25*avatarScale[2]])

def sendMainCameraHome(gallery, window):
    global linkedAvatarId
    global avatar_link
    info.visible(viz.OFF)
    info2.visible(viz.OFF)
    if camLinks.has_key(window.getView().id):
        camLinks[window.getView().id].remove()
    linkedAvatarId = -1
    bs=gallery.getBoundingSphere()
    c = bs.getCenter()
    r = bs.getRadius()
    viz.MainView.setPosition([c[0],c[1]+r,c[2]-r])
    viz.MainView.setEuler([0,30,0])

def overviewCam(location):
    global avatar_link
    info.visible(viz.OFF)
    info2.visible(viz.OFF)
    avatar_link.disable()
    fn = vizcam.FlyNavigate()
    fn.sensitivity(10,1)
    viz.cam.setHandler(fn)
    sendMainCameraHome(location)
    viz.cam.setReset()
    viz.cam.reset()

def SetFTNCam(gallery): #fly to navigate
    #cam-setup
    fn = vizcam.FlyNavigate()
    fn.sensitivity(10, 1)
    viz.cam.setHandler(fn)
    #standard position
    sendMainCameraHome(gallery, viz.MainWindow)
    viz.cam.setReset()


def toggleLink(link):
	if link.getEnabled():
		link.disable()
	else:
		link.enable()


def toggleTimer(timer):
    timer.setEnabled(not timer.getEnabled())
