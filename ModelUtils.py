﻿import ModelCreater
import jsonParser
import DSObjectClass
import FadeInOutThread
import ModelClass
import viz
import vizact
import time

visitedPolygons = []
routeLines = []

def CreateIsovistModels(isovistData, r,g,b, alpha, height):
    retVal = []
    for id in isovistData.ids:
        newModel = ModelCreater.Create3dModelTriangleFan(isovistData.contours[id],isovistData.triggers[id], height, 0)
        newModel.visible(viz.OFF)
        newModel.color(r,g,b)
        newModel.alpha(alpha)
        retVal.append(newModel)
    return retVal


def createDesignObjects(jsonFile):
    dataSet = jsonParser.getObjectData(jsonFile)
    dsObjects = []
    for d in dataSet:
        newObj = DSObjectClass.DSObject(d)
        dsObjects.append(newObj)
        
    return dsObjects


def create3dPolygons(jsonFile, artefactType, r,g,b, alpha):
    data = jsonParser.getArtefactData(jsonFile, artefactType)
    retVal = []
    for d in data:
        newModel = ModelClass.Model(d["guid"])
        newModel.Create3dModel(d["contours"], d["holes"], d["height"], d["elevation"])
        newModel.model.color(r,g,b)
        newModel.model.alpha(alpha)
        newModel.model.visible(viz.OFF)
        retVal.append(newModel)
    return retVal
    
def create2dPolygons(jsonFile, artefactType, r,g,b, alpha):
    data = jsonParser.getArtefactData(jsonFile, artefactType)
    retVal = []
    for d in data:
        newModel = ModelClass.Model(d["guid"])
        newModel.Create2dModel(d["contours"], d["holes"], d["elevation"])
        newModel.model.color(r,g,b)
        newModel.model.alpha(alpha)
        newModel.model.visible(viz.OFF)
        retVal.append(newModel)
    return retVal
    
def CreateWireFrames(jsonFile, artefactType, r,g,b, alpha):
    data = jsonParser.getArtefactData(jsonFile, artefactType)
    retVal = []
    for d in data:
        newModel = ModelClass.Model(d["guid"])
        newModel.CreateWireFrame(d["contours"], d["height"], d["elevation"])
        newModel.wireFrameModel.color(r,g,b)
        newModel.wireFrameModel.alpha(alpha)
        newModel.wireFrameModel.visible(viz.OFF)
        retVal.append(newModel)
    return retVal


def setVisitedMovementSpaces(polygonModels, avatars):
	for a in avatars:
		pos = a.vizAvatar.getPosition()
		wasInside = False
		for p in polygonModels:
			if p.polygon.isInside(pos[0], pos[2]):
				a.movementSpaceId = p.guid
				wasInside = True
				if len(visitedPolygons) <= a.id:
					modelToAdd = [p]
					visitedPolygons.append(modelToAdd)
				elif visitedPolygons[a.id].count(p) == 0:
					visitedPolygons[a.id].append(p)
		if not wasInside:
			a.movementSpaceId = None

def showMovementSpacesCurrent(polygonModels, avatars):
    setVisitedMovementSpaces(polygonModels, avatars)
    for list in visitedPolygons:
        for model in list:
            model.model.visible(False)

	for a in avatars:
		if a.movementSpaceId != None:
			visitedPolygons[a.id][len(visitedPolygons[a.id])-1].model.visible(True)

def showMovementSpacesTrace(polygonModels, avatars):
    setVisitedMovementSpaces(polygonModels, avatars)
    for list in visitedPolygons:
        for model in list:
            model.model.visible(True)


# fading models in and out
def fadeModelIn(model):
    """Should be called in a new thread."""
    if not model.getVisible():
        model.visible(viz.ON)
        model.alpha(0.01)
        currentAlpha = 0.01
        alphaFinale = 0.2
        while currentAlpha < alphaFinale:
            currentAlpha += 0.005
            model.alpha(currentAlpha)
            time.sleep(0.05)

def fadeModelOut(model):
	"""Should be called in a new thread."""
	if model.getVisible():
		currentAlpha = model.getAlpha()
		while currentAlpha > 0:
			currentAlpha -= 0.005
			if currentAlpha < 0:
				currentAlpha = 0
			model.alpha(currentAlpha)
			time.sleep(0.05)
		model.visible(viz.OFF)

def ModelById(models, id):
    for m in models:
        if m.guid == id:
            return m

def listContains(list, object):
    for i in list:
        if object == i:
            return True
    return False


def MakeObjectsVisible(avatar, designModels):
    """Makes the objects visible, that are visible to an avatar in the avatars isovist."""
    ids = avatar.isovists.visibleProducts[avatar.nextIsovistTrigger]
    for id in ids:
        if not listContains(avatar.visibleObjectsIds, id):
            avatar.visibleObjectsIds.append(id)
            model = ModelById(designModels, id).polygon[0]
            thread = FadeInOutThread.FadeInThread(model) #fade in
            thread.start()


def MakeObjectsVisibleWithNodes(avatar, designModels, osgNodes):
    """Makes the objects visible, that are visible to an avatar in the avatars isovist."""
    ids = avatar.isovists.visibleProducts[avatar.nextIsovistTrigger]
    for id in ids:
        if not listContains(avatar.visibleObjectsIds, id):
            avatar.visibleObjectsIds.append(id)
            model = ModelById(designModels, id)
            thread = FadeInOutThread.FadeInThread(model.polygon[0]) #fade in
            if model.ifc_class_type != 'IfcOpeningElement':
                osgNodes[model.guid].visible(True)
            thread.start()



def MakeObjectsInvisible(avatar ,designModels, avatars):
    """Makes the objects invisible, that are no longer visible in the avatars isovist"""
    ids = avatar.isovists.visibleProducts[avatar.nextIsovistTrigger]
    allVisibleObjects = set()
    currentVisibleObjects = set()
    for a in avatars:
        for id in a.visibleObjectsIds:
            allVisibleObjects.add(id)
        for id in a.isovists.visibleProducts[a.nextIsovistTrigger]:
            currentVisibleObjects.add(id)
    for id in allVisibleObjects:
        if not listContains(currentVisibleObjects, id):
            if listContains(avatar.visibleObjectsIds, id):
                avatar.visibleObjectsIds.pop(avatar.visibleObjectsIds.index(id)) #remove from the visible list
                model = ModelById(designModels, id).polygon[0]
                thread = FadeInOutThread.FadeOutThread(model) #fade out
                thread.start()



def LinesToVisibleObjects(avatar, designModels, r,g,b, alpha):
    lines = []
    ids = avatar.isovists.visibleProducts[avatar.nextIsovistTrigger]
    for id in ids:
        if not listContains(avatar.visibleLinesIds, id):
            avatar.visibleLinesIds.append(id)
            model = ModelById(designModels, id)
            line = ModelClass.Model(id)
            line.CreateLine(avatar.vizAvatar.getBoundingBox().getCenter(), model.polygon[0].getBoundingBox().getCenter())
            line.LineModel.color(r,g,b)
            line.LineModel.alpha(alpha)
            line.LineModel.visible(viz.OFF)
            thread = FadeInOutThread.FadeInThread(line.LineModel)
            thread.start()
            vizact.ontimer(0, updateLine, line, avatar)
            avatar.linesToObjects.append(line)
    RemoveLinesForNotLongerVisibleObjects(avatar, avatar.linesToObjects)

def updateLine(line, avatar):
        ModelCreater.UpdateLine(line.LineModel, avatar.vizAvatar.getBoundingBox().getCenter())

def DrawLineFromTriggerToObject(avatar, designModels, triggerPosition, r,g,b, alpha):
	ids = avatar.isovists.visibleProducts[avatar.nextIsovistTrigger]
	for id in ids:
		if not listContains(avatar.linesFromTriggerToObjectIds, id):
			avatar.linesFromTriggerToObjectIds.append(id)
			model = ModelById(designModels, id).polygon[0]
			line = ModelClass.Model(id)
			line.CreateLine(triggerPosition, model.getBoundingBox().getCenter())
			line.LineModel.color(r,g,b)
			line.LineModel.alpha(alpha)
			avatar.linesFromTriggerToObject.append(line)

def RemoveLinesForNotLongerVisibleObjects(avatar, lines):
    ids = avatar.isovists.visibleProducts[avatar.nextIsovistTrigger]
    lineId = []
    for l in lines:
        lineId.append(l.guid)
    for vid in avatar.visibleLinesIds:
        if not listContains(ids, vid):
            avatar.visibleLinesIds.pop(avatar.visibleLinesIds.index(vid))
            line = ModelById(lines, vid)
            avatar.linesToObjects.pop(avatar.linesToObjects.index(line))
            line.LineModel.remove()


def fadeAllModelsOut(models):
    for m in models:
        thread = FadeInOutThread.FadeOutThread(m.model)
        thread.start()
        
def fadeAllLinesOut(lines):
    for l in lines:
        thread = FadeInOutThread.FadeOutThread(l.LineModel)
        thread.start()

def fadeAllLinesIn(lines):
    for l in lines:
        thread = FadeInOutThread.FadeInThread(l.LineModel)
        thread.start()


def ModelAsWireFrame(model):
    newModel = ModelClass.Model(model.id)
    newModel.CreateWireFrame(model.footprint, model.height, model.elevation)
    return newModel


#make all models visible or invisible 
def AllInvisible(models):
    for m in models:
        if isinstance(m, ModelClass.Model):
            m.model.visible(viz.OFF)
        elif isinstance(m, DSObjectClass.DSObject) and not m.polygon is None:
            print m.polygon
            m.polygon[0].visible(viz.OFF)

def AllVisible(models):
    for m in models:
        if isinstance(m, ModelClass.Model):
            m.model.visible(viz.ON)
        elif isinstance(m, DSObjectClass.DSObject) and not m.polygon is None:
            m.polygon[0].visible(viz.ON)

def AllMappedNodesVisible(mappedNodes):
    for n in mappedNodes:
        mappedNodes[n].visible(True)

def AllMappedNodesInvisible(mappedNodes):
    for n in mappedNodes:
        mappedNodes[n].visible(False)

def toggleAllMappedNodes(mappedNodes):
    for n in mappedNodes:
        mappedNodes[n].visible(not mappedNodes[n].getVisible())

def toggleVisible(model):
    if isinstance(model, ModelClass.Model):
        model.model.visible(not model.model.getVisible())
    elif isinstance(model, DSObjectClass.DSObject) and not model.polygon is None:
        model.polygon[0].visible(not model.polygon[0].getVisible())
    
def toggleAllVisible(models):
    for m in models:
        if isinstance(m, ModelClass.Model):
            m.model.visible(not m.model.getVisible())
        elif isinstance(m, DSObjectClass.DSObject) and not m.polygon is None:
            m.polygon[0].visible(not m.polygon[0].getVisible())

def toggleIsovists(avatars):
    for a in avatars:
        a.ToggleIsovists()

def toggleMovementSpaceMode(timer0, timer1):
    timer0.setEnabled(not timer0.getEnabled())
    timer1.setEnabled(not timer1.getEnabled())


def AddLineLayers(windows, avatars, r,g,b, alpha):
    global lines
    for a in avatars:	
        viz.startLayer(viz.LINE_STRIP)
        viz.vertexColor(r,g,b)
        lines = viz.endLayer()
        lines.renderOnlyToWindows(windows)
        lines.alpha(alpha)
        lines.dynamic() 
        routeLines.append(lines)


def DrawPathToWindow(avatars):
    for a in avatars:
        x,y,z = a.vizAvatar.getPosition()
        lx,ly,lz = lines.getVertex(-1)
        xdif = abs(x-lx)
        ydif = abs(y-ly)
        if x != lx or y != ly:
            routeLines[a.id].addVertex([x,y+0.1,z])

