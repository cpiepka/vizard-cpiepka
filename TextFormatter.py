﻿#format text for infopanel
def formatText(text, lineBreakAfterWords):
    splitted = text.split()
    i = lineBreakAfterWords
    while i < len(splitted):
        splitted.insert(i, '\n')
        i += lineBreakAfterWords
    retText = ' '.join(splitted)
    return retText
