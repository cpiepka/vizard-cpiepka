﻿import DesignUtils
import CameraUtils
import AvatarUtils
import ModelUtils
import viz
import vizact
import WindowsSetupUtils

gulbenkian = DesignUtils.load_gulbenkian_no_roof()

viz.MainWindow.fov(60)
viz.setMultiSample(4)
viz.clearcolor(viz.SKYBLUE)
viz.go()

CameraUtils.SetFTNCam(gulbenkian)

avatars = AvatarUtils.CreateAvatars('dsim-json/Gulbenkian/IsovistNarrativeRoutes2.JSON', 1.0, 4.59999275208, 'vcc_male.cfg')
ModelUtils.toggleIsovists(avatars)

windows = WindowsSetupUtils.SplitScreenVert()

vizact.onkeydown('1', CameraUtils.linkCamBehind, avatars[0], viz.MainWindow)
vizact.onkeydown('2', CameraUtils.linkCamBehind, avatars[1], windows[0])

vizact.onkeydown('3', CameraUtils.linkCamFront, avatars[0], viz.MainWindow)
vizact.onkeydown('4', CameraUtils.linkCamFront, avatars[1], windows[0])

vizact.onkeydown('5', CameraUtils.linkCamTopDown, avatars[0], viz.MainWindow)
vizact.onkeydown('6', CameraUtils.linkCamTopDown, avatars[1], windows[0])

vizact.onkeydown(viz.KEY_BACKSPACE, CameraUtils.sendMainCameraHome, gulbenkian, viz.MainWindow)

vizact.onkeydown(viz.KEY_KP_ADD, CameraUtils.zoomCam, True, viz.MainWindow)
vizact.onkeydown(viz.KEY_KP_SUBTRACT, CameraUtils.zoomCam, False, viz.MainWindow)


