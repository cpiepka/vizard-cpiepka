﻿import vizshape
import viz
import vizcam
import Polygon

def Create3dModel(footprint, height, elevation):
	#creating top and bottom
	i = 2
	while i > 0:
		viz.startLayer(viz.POLYGON)
		i -= 1
		for f in footprint:
			if i > 0:
				viz.vertex(f[0], elevation, f[2])
			else:
				viz.vertex(f[0], elevation+height, f[2])
	#creating walls
	while i < len(footprint)-1:
		viz.startLayer(viz.QUAD_STRIP)
		viz.vertex(footprint[i][0],elevation,footprint[i][2])
		viz.vertex(footprint[i+1][0],elevation,footprint[i+1][2])
		viz.vertex(footprint[i][0],elevation+height,footprint[i][2])
		viz.vertex(footprint[i+1][0],elevation+height,footprint[i+1][2])
		i += 1
	#creating last wall
	viz.startLayer(viz.QUAD_STRIP)
	viz.vertex(footprint[i][0],elevation,footprint[i][2])
	viz.vertex(footprint[0][0],elevation,footprint[0][2])
	viz.vertex(footprint[i][0],elevation+height,footprint[i][2])
	viz.vertex(footprint[0][0],elevation+height,footprint[0][2])
	
	retVal = viz.endLayer()
	retVal.optimize()
	return retVal
	
def Create3dModelTriangele(footprint, triangles, height, elevation):
	#creating top and bottom
	i = 2
	while i > 0:
		viz.startLayer(viz.TRIANGLES)
		i -= 1
		for t0 in triangles:
			for t in t0:
				if i > 0:
					#viz.vertex(t[0], elevation, t[2])
					pass
				else:
					viz.vertex(t[0], elevation+height, t[2])
	#creating walls
	while i < len(footprint)-1:
		viz.startLayer(viz.QUAD_STRIP)
		viz.vertex(footprint[i][0],elevation,footprint[i][2])
		viz.vertex(footprint[i+1][0],elevation,footprint[i+1][2])
		viz.vertex(footprint[i][0],elevation+height,footprint[i][2])
		viz.vertex(footprint[i+1][0],elevation+height,footprint[i+1][2])
		i += 1
	#creating last wall
	viz.startLayer(viz.QUAD_STRIP)
	viz.vertex(footprint[i][0],elevation,footprint[i][2])
	viz.vertex(footprint[0][0],elevation,footprint[0][2])
	viz.vertex(footprint[i][0],elevation+height,footprint[i][2])
	viz.vertex(footprint[0][0],elevation+height,footprint[0][2])
	
	retVal = viz.endLayer()
	retVal.optimize()
	return retVal
	
def Create3dModelTriangleFan(footprint, trigger, height, elevation):
	i = 2
	while i > 0:
		viz.startLayer(viz.TRIANGLE_FAN)
		viz.vertex(trigger[0], elevation+height, trigger[2])
		i -= 1
		for f in footprint:
			if i > 0:
				#viz.vertex(f[0], elevation, f[2])
				pass
			else:
				viz.vertex(f[0], elevation+height, f[2])
	#creating walls
	while i < len(footprint)-1:
		viz.startLayer(viz.QUAD_STRIP)
		viz.vertex(footprint[i][0],elevation,footprint[i][2])
		viz.vertex(footprint[i+1][0],elevation,footprint[i+1][2])
		viz.vertex(footprint[i][0],elevation+height,footprint[i][2])
		viz.vertex(footprint[i+1][0],elevation+height,footprint[i+1][2])
		i += 1
	#creating last wall
	viz.startLayer(viz.QUAD_STRIP)
	viz.vertex(footprint[i][0],elevation,footprint[i][2])
	viz.vertex(footprint[0][0],elevation,footprint[0][2])
	viz.vertex(footprint[i][0],elevation+height,footprint[i][2])
	viz.vertex(footprint[0][0],elevation+height,footprint[0][2])
	
	retVal = viz.endLayer()
	retVal.optimize()
	return retVal
	
def CreateWire(footprint, height, elevation):
	i = 2
	while i > 0:
		viz.startLayer(viz.LINE_STRIP)
		i -= 1
		for f in footprint:
			if i > 0:
				viz.vertex(f[0], elevation, f[2])
			else:
				viz.vertex(f[0], elevation+height, f[2])
	#creating walls
	while i < len(footprint)-1:
		viz.startLayer(viz.LINE_STRIP)
		viz.vertex(footprint[i][0],elevation,footprint[i][2])
		viz.vertex(footprint[i][0],elevation+height,footprint[i][2])
		viz.vertex(footprint[i+1][0],elevation+height,footprint[i+1][2])
		viz.vertex(footprint[i+1][0],elevation,footprint[i+1][2])
		i += 1
	
	retVal = viz.endLayer()
	retVal.optimize()
	retVal.color(viz.RED)
	return retVal
	
def CreateLine(point0, point1):
	"""Creates a line between to points"""
	viz.startLayer(viz.LINES)
	
	viz.vertex(point0[0],point0[1],point0[2])
	viz.vertex(point1[0],point1[1],point1[2])
	
	retVal = viz.endLayer()
	
	retVal.dynamic()
	retVal.optimize()
	retVal.color(viz.RED)
	return retVal
	
def UpdateLine(line, point):
	line.setVertex(0, point[0], point[1], point[2])
	
	
def CreatePolygon(contour, holes, hight, elevation):
	contour2d = []
	holes2d = []
	
	if holes == None:
		holes = []
	
	for c in contour:
		contour2d.append([c[0], c[2]])
		
	for h in holes:
		newHole = []
		for point in h:
			newHole.append([point[0], point[2]])
		holes2d.append(newHole)
			
	cPoly = Polygon.Polygon(contour2d)
	
	for h in holes2d:
		cPoly = cPoly - Polygon.Polygon(h)

	triangleStrip = cPoly.triStrip()
	
	for t in triangleStrip:
		viz.startLayer(viz.TRIANGLE_STRIP)
		for point in t:
			viz.vertex(point[0], 0.01, point[1])
	
	retPoly = viz.endLayer()
	retVal = []
	retVal.append(retPoly)
	retVal.append(cPoly)
	return retVal
	
	
def Create2DPolygon(contours, holes, elevation):
	contour2d = []
	holes2d = []
	
	if holes == None:
		holes = []
	
	for contour in contours:
		newContour = []
		for point in contour:
			newContour.append([point[0], point[2]])
		contour2d.append(newContour)

	for h in holes:
		newHole = []
		for point in h:
			newHole.append([point[0], point[2]])
		holes2d.append(newHole)

	cPoly = Polygon.Polygon()
	
	for c in contour2d:
		cPoly.addContour(c)
	
	for h in holes2d:
		cPoly = cPoly - Polygon.Polygon(h)

	triangleStrip = cPoly.triStrip()
	
	for t in triangleStrip:
		viz.startLayer(viz.TRIANGLE_STRIP)
		for point in t:
			viz.vertex(point[0], elevation+0.01, point[1])
	
	retPoly = viz.endLayer()
	retVal = []
	retVal.append(retPoly)
	retVal.append(cPoly)
	return retVal
	
def Create3DPolygon(contours, holes, height, elevation):
	contour2d = []
	holes2d = []
	
	if holes == None:
		holes = []
	
	for contour in contours:
		newContour = []
		for point in contour:
			newContour.append([point[0], point[2]])
		contour2d.append(newContour)

	for h in holes:
		newHole = []
		for point in h:
			newHole.append([point[0], point[2]])
		holes2d.append(newHole)

	cPoly = Polygon.Polygon()
	
	for c in contour2d:
		cPoly.addContour(c)
	
	for h in holes2d:
		cPoly = cPoly - Polygon.Polygon(h)

	triangleStrip = cPoly.triStrip()
	
	for t in triangleStrip:
		viz.startLayer(viz.TRIANGLE_STRIP)
		for point in t:
			viz.vertex(point[0], elevation+0.01, point[1])
			
	for t in triangleStrip:
		viz.startLayer(viz.TRIANGLE_STRIP)
		for point in t:
			viz.vertex(point[0], height+elevation+0.01, point[1])
	
	for c in contour2d:
		i = 0
		while i < len(c)-1:
			viz.startLayer(viz.QUAD_STRIP)
			viz.vertex(c[i][0],elevation,c[i][1])
			viz.vertex(c[i+1][0],elevation,c[i+1][1])
			viz.vertex(c[i][0],elevation+height,c[i][1])
			viz.vertex(c[i+1][0],elevation+height,c[i+1][1])
			i += 1
	#creating last wall
		viz.startLayer(viz.QUAD_STRIP)
		viz.vertex(c[i][0],elevation,c[i][1])
		viz.vertex(c[0][0],elevation,c[0][1])
		viz.vertex(c[i][0],elevation+height,c[i][1])
		viz.vertex(c[0][0],elevation+height,c[0][1])
	
	retPoly = viz.endLayer()
	retPoly.alpha(0.2)
	retPoly.color(viz.RED)
	retPoly.visible(False)
	
	retVal = []
	retVal.append(retPoly)
	retVal.append(cPoly)
	return retVal
