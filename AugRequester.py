﻿import socket

class AugRequester:
	def __init__(self):
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
	
	def connect(self, ip, port):
		self.socket.connect((ip, port))
	
	def close(self):
		self.socket.close()
	
	def send(self, msg):
		sent = self.socket.send(msg)
		if sent == 0:
			raise RuntimeError("socket connection broken")
	
	def receive(self):
		chunk = self.socket.recv(8192)
		if chunk == '':
			raise RuntimeError("socket connection broken")
		return chunk
	
	def askForPath(self, startPoint, endPoint, modelName):
		#example string: 'path gulbenkian 1 0 3 4 0 6'
		posString = str(startPoint[0]) + ' ' + str(startPoint[1])+ ' ' + str(startPoint[2])+ ' ' + str(endPoint[0])+ ' ' + str(endPoint[1])+ ' ' + str(endPoint[2])
		print 'path '+ modelName + ' ' + posString
		self.send('path '+ modelName + ' ' + posString)
		return self.receive()
	




