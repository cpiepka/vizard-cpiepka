﻿import ModelUtils
import DesignUtils
import CameraUtils
import AvatarUtils
import viz
import vizact

gulbenkian = DesignUtils.load_gulbenkian_no_roof()

viz.MainWindow.fov(60)
viz.setMultiSample(4)
viz.clearcolor(viz.SKYBLUE)
viz.go()

CameraUtils.SetFTNCam(gulbenkian)

functionalSpaces = ModelUtils.create3dPolygons('dsim-json/Gulbenkian/Artefacts.json', 'Functional Spaces', 0,1,0.5, 0.5)
operationalSpaces = ModelUtils.create3dPolygons('dsim-json/Gulbenkian/Artefacts.json', 'Operational Spaces', 1,0.5,0, 0.5)

vizact.onkeydown('f', ModelUtils.toggleAllVisible, functionalSpaces)
vizact.onkeydown('o', ModelUtils.AllVisible, operationalSpaces)

vizact.onkeydown('p', vizact.ontimer2,5,0,ModelUtils.AllInvisible,operationalSpaces)

