﻿import DesignUtils
import CameraUtils
import AvatarUtils
import ModelUtils
import TextFormatter
import WindowsSetupUtils
import viz
import vizact

gulbenkian = DesignUtils.load_normain('dsim-json\Normain-Interchange\Norman-Interchange-July 5-2013.OSGB')

viz.MainWindow.fov(60)
viz.setMultiSample(4)
viz.clearcolor(viz.SKYBLUE)
viz.go()
windows = WindowsSetupUtils.SplitScreenVert()
CameraUtils.SetFTNCam(gulbenkian)

avatars = AvatarUtils.CreateAvatars('dsim-json/Normain-Interchange/IsovistNarrativeRoutes-Floor1and2-a.JSON', 1.0, 4.59999275208, 'vcc_male.cfg')
ModelUtils.toggleIsovists(avatars)

def onNarrative(avatarId, text):
	if CameraUtils.linkedAvatarId == avatarId:
		CameraUtils.info.visible(viz.ON)
		CameraUtils.info.setText(TextFormatter.formatText(text, 8))

def onUserState(avatar, text, state):
	avatar.setAvatarState(state)
	if CameraUtils.linkedAvatarId == avatar.id:
		CameraUtils.info2.visible(viz.ON)
		CameraUtils.info2.setText(text)
		vizact.ontimer2(avatar.vizAvatar.getDuration(state), 0, CameraUtils.info2.visible, viz.OFF)

for a in avatars:
	a.vizAvatar.setScale(2,2,2)
	a.enableEvent('onNarrative', onNarrative) #narrative-event
	a.enableEvent('onAsk', onUserState) #ask-event
	a.enableEvent('onWaits', onUserState) #wait-event
	a.enableEvent('onFrustrated', onUserState) #frustrated-event
	a.enableEvent('onLooksAround', onUserState) #looks-around-event

vizact.onkeydown('1', CameraUtils.linkCamBehind, avatars[0], viz.MainWindow)
vizact.onkeydown('2', CameraUtils.linkCamBehind, avatars[1], windows[0])

vizact.onkeydown('3', CameraUtils.linkCamFront, avatars[0], viz.MainWindow)
vizact.onkeydown('4', CameraUtils.linkCamFront, avatars[1], windows[0])

vizact.onkeydown('5', CameraUtils.linkCamTopDown, avatars[0], viz.MainWindow)
vizact.onkeydown('6', CameraUtils.linkCamTopDown, avatars[1], windows[0])

vizact.onkeydown(viz.KEY_BACKSPACE, CameraUtils.sendMainCameraHome, gulbenkian, viz.MainWindow)

vizact.onkeydown(viz.KEY_KP_ADD, CameraUtils.zoomCam, True, viz.MainWindow)
vizact.onkeydown(viz.KEY_KP_SUBTRACT, CameraUtils.zoomCam, False, viz.MainWindow)

vizact.onkeydown('s', AvatarUtils.speedUp, avatars)
