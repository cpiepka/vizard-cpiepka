﻿import viz
import DesignUtils
import CameraUtils
import ModelUtils
import AvatarUtils
import OSGMapper
import vizact

viz.MainWindow.fov(60)
viz.setMultiSample(4)
viz.clearcolor(viz.SKYBLUE)
viz.go()


gulbenkian = DesignUtils.load_gulbenkian_no_roof()

CameraUtils.SetFTNCam(gulbenkian)

designObjects = ModelUtils.createDesignObjects('dsim-json\Gulbenkian\Gulbenkian.ifc.JSON')

mapped = OSGMapper.mapObjectsToOSGNodes(designObjects, gulbenkian, DesignUtils.get_gulbenkian_scale())

ModelUtils.AllMappedNodesInvisible(mapped)
	
#Avatars
avatars = AvatarUtils.CreateAvatars('dsim-json/Gulbenkian/IsovistNarrativeRoutes2.JSON', 1.0, 4.59999275208, 'vcc_male.cfg') #avatars have isovists

ModelUtils.toggleIsovists(avatars)

routeGraph = ModelUtils.create2dPolygons('dsim-json/Gulbenkian/Artefacts.json', 'Route Graph V3', 0.9,0.9,0.9, 0.4)
routeGraph.pop(len(routeGraph)-1)
routeGraph.pop(len(routeGraph)-1)
routeGraph.pop(len(routeGraph)-1)

ModelUtils.AllVisible(routeGraph)

ModelUtils.AddLineLayers([viz.MainWindow], avatars, 0,0,1, 1)
vizact.ontimer(0,ModelUtils.DrawPathToWindow, avatars)

def onIsovist(avatar, triggerPosition):
	ModelUtils.MakeObjectsVisibleWithNodes(avatar, designObjects, mapped)
	ModelUtils.MakeObjectsInvisible(avatar, designObjects, avatars)
	#ModelUtils.LinesToVisibleObjects(avatar, designObjects, 0,0.7,0, 0.2)
	#ModelUtils.DrawLineFromTriggerToObject(avatar, designObjects, triggerPosition, 0,0,1, 0.2)

#
for a in avatars:
    a.enableEvent('onIsovist', onIsovist) #isovist-event

		
#Control speed
vizact.onkeydown(viz.KEY_KP_ADD, AvatarUtils.speedUp, avatars)
vizact.onkeydown(viz.KEY_KP_SUBTRACT, AvatarUtils.speedDown, avatars)
vizact.onkeydown('p', AvatarUtils.speedPause, avatars)



