﻿import viz
import data_scripts
import utils
import vizact

viz.MainWindow.fov(60)
viz.setMultiSample(4)
viz.clearcolor(viz.SKYBLUE)
viz.go()


gulbenkian = data_scripts.load_gulbenkian_no_roof()

utils.SetFTNCam(gulbenkian)

designObjects = utils.createDSObjects('dsim-json\Gulbenkian\Gulbenkian.ifc.JSON')


childrenList = []
for name in gulbenkian.getNodeNames():
	child = gulbenkian.getChild(name)
	if child.id > -1:
		childrenList.append(child)

mapped = utils.mapObjectsToOSGNodes(designObjects, childrenList, data_scripts.get_gulbenkian_scale())

for n in mapped:
	mapped[n].visible(False)
	
#Avatars
utils.CreateAvatars('dsim-json/Gulbenkian/IsovistNarrativeRoutes2.JSON', 1.0, 4.59999275208) #avatars have isovists
for a in utils.avatars: #isovists off
        a.ToggleIsovists()

routeGraph = utils.CreatePolygonModels('dsim-json/Gulbenkian/Artefacts.json', 'Route Graph V3', 0.9,0,0, 0.2)
routeGraph.pop(len(routeGraph)-1)
routeGraph.pop(len(routeGraph)-1)
routeGraph.pop(len(routeGraph)-1)

for r in routeGraph:
	r.model.visible(True)
	


def onIsovist(avatar):
    utils.MakeObjectsVisibleWithNodes(avatar, designObjects, mapped)
    utils.MakeObjectsInvisibleNew(avatar, designObjects)
    utils.LinesToVisibleObjectsNew(avatar, designObjects, 0,0,0.7, 0.2)
    

#
for a in utils.avatars:
    a.enableEvent('onIsovist', onIsovist) #isovist-event

		
#Controlling speed
vizact.onkeydown(viz.KEY_KP_ADD, utils.speedUp, utils.avatars)
vizact.onkeydown(viz.KEY_KP_SUBTRACT, utils.speedDown, utils.avatars)
vizact.onkeydown('p', utils.speedPause, utils.avatars)



#########testing mapping##############		
#i = 1
#def nextModel():
#	global i
#	if not designObjects[i].polygon is None:
#		designObjects[i].polygon[0].visible(True)
#		mapped[designObjects[i].guid].visible(True)
#		#print mapped[designObjects[i].guid]
#	if not designObjects[i-1].polygon is None:
#		designObjects[i-1].polygon[0].visible(False)
#		#mapped[designObjects[i-1].guid].visible(False)
#	i+=1
#	
#	
#vizact.onkeydown('n', nextModel)

