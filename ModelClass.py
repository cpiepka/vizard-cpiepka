﻿import ModelCreater

class Model():
	def Create3dModel(self, footprint, holes, height, elevation):
		createdPolygon = ModelCreater.Create3DPolygon(footprint, holes, height, elevation)
		self.model = createdPolygon[0]
		self.footprint = footprint
		self.holes = holes
		self.height = height
		self.elevation = elevation
		self.polygon = createdPolygon[1]
		return self.model
	
	def Create2dModel(self, footprint, holes, height):
		createdPolygon = ModelCreater.Create2DPolygon(footprint, holes, height)
		self.model = createdPolygon[0]
		self.footprint = footprint
		self.holes = holes
		self.height = height
		self.polygon = createdPolygon[1]
		return self.model
		
	def CreateLine(self, position0, position1):
		self.LineModel = ModelCreater.CreateLine(position0, position1)
		
	def CreateWireFrame(self, footprint, height, elevation):
		self.wireFrameModel = ModelCreater.CreateWire(footprint, height, elevation)
		self.footprint = footprint
		self.height = height
		self.elevation = elevation
		return self.wireFrameModel
	
	def __init__(self, id):
		self.guid = id