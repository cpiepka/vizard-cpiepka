﻿import viz
import vizact
import jsonParser
import IsovistClass
import math
import ModelUtils

class Avatar(viz.EventClass):
	def enableEvent(self, event, handlingMethod):
		""" Enables an event. 
		param event as a string: 
			onNarrative
			onVisible
			onAsk
			onFrustrated
			onLooksAround
			onWaits
			onIsovist"""
		if event == 'onNarrative':
			self.narrativeEnabled = True
			self.callback(self.AVATAR_ON_NARRATIVE_TRIGGER_EVENT, handlingMethod)
		elif event == 'onVisible':
			self.visibleEnabled = True
			self.callback(self.AVATAR_ON_VISIBLE_TRIGGER_EVENT, handlingMethod)
		elif event == 'onAsk':
			self.askEnabled = True
			self.callback(self.AVATAR_ON_ASK_TRIGGER_EVENT, handlingMethod)
		elif event == 'onFrustrated':
			self.frustratedEnabled = True
			self.callback(self.AVATAR_ON_FRUSTRATED_TRIGGER_EVENT, handlingMethod)
		elif event == 'onLooksAround':
			self.looksAroundEnabled = True
			self.callback(self.AVATAR_ON_LOOKSAROUND_TRIGGER_EVENT, handlingMethod)
		elif event == 'onWaits':
			self.waitsEnabled = True
			self.callback(self.AVATAR_ON_WAITS_TRIGGER_EVENT, handlingMethod)
		elif event == 'onIsovist':
			self.isovistEnabled = True
			self.callback(self.AVATAR_ON_ISOVIST_TRIGGER_EVENT, handlingMethod)
		else:
			raise Exception(event + ' is unknown as an event.')
			
			
	def disableEvent(self, event):
		""" Disables an event. 
		param event as a string: 
			onNarrative
			onVisible
			onAsk
			onFrustrated
			onLooksAround
			onWaits
			onIsovist"""
		if event == 'onNarrative':
			self.narrativeEnabled = False
		elif event == 'onVisible':
			self.visibleEnabled = False
		elif event == 'onAsk':
			self.askEnabled = False
		elif event == 'onFrustrated':
			self.frustratedEnabled = False
		elif event == 'onLooksAround':
			self.looksAroundEnabled = False
		elif event == 'onWaits':
			self.waitsEnabled = False
		elif event == 'onIsovist':
			self.isovistEnabled = False
		else:
			raise Exception(event + ' is unknown as an event.')
			
	def distance(self, point0, point1):
		return math.sqrt((point0[0]-point1[0]) * (point0[0]-point1[0])
		+(point0[2]-point1[2]) * (point0[2]-point1[2]))
		
	def checkPositionEvents(self):
		position = [self.vizAvatar.getPosition()[0],self.vizAvatar.getPosition()[1],self.vizAvatar.getPosition()[2]]
		if self.nextNarrativeTrigger < len(self.narrativTriggers):	
			narrativeTriggerPosition = self.narrativTriggers[self.nextNarrativeTrigger]
			if self.distance(position, narrativeTriggerPosition) < 0.8:
				self.nextNarrativeTrigger += 1
				if self.narrativeEnabled:
					viz.sendEvent(self.AVATAR_ON_NARRATIVE_TRIGGER_EVENT, self.id, self.narrativTexts[self.nextNarrativeTrigger-1])
			
		if self.nextVisibleTrigger < len(self.visibleTriggers):
			visibleTriggerPosition = self.visibleTriggers[self.nextVisibleTrigger]
			if self.distance(position, visibleTriggerPosition) < 0.8:
				self.nextVisibleTrigger += 1
				if self.visibleEnabled:
					viz.sendEvent(self.AVATAR_ON_VISIBLE_TRIGGER_EVENT, self)
			
		if self.nextAskTrigger < len(self.askTriggers):
			askTriggerPosition = self.askTriggers[self.nextAskTrigger]
			if self.distance(position, askTriggerPosition) < 0.8:
				self.nextAskTrigger += 1
				if self.askEnabled:
					viz.sendEvent(self.AVATAR_ON_ASK_TRIGGER_EVENT, self, self.askTexts[self.nextAskTrigger-1], 14)
				
		if self.nextFrustratedTrigger < len(self.frustratedTriggers):
			frustratedTriggerPosition = self.frustratedTriggers[self.nextFrustratedTrigger]
			if self.distance(position, frustratedTriggerPosition) < 0.8:
				self.nextFrustratedTrigger += 1
				if self.frustratedEnabled:
					viz.sendEvent(self.AVATAR_ON_FRUSTRATED_TRIGGER_EVENT, self, self.frustratedTexts[self.nextFrustratedTrigger-1], 3)
				
		if self.nextLooksAroundTrigger < len(self.looksAroundTriggers):
			looksAroundTriggerPosition = self.looksAroundTriggers[self.nextLooksAroundTrigger]
			if self.distance(position, looksAroundTriggerPosition) < 0.8:
				self.nextLooksAroundTrigger += 1
				if self.looksAroundEnabled:
					viz.sendEvent(self.AVATAR_ON_LOOKSAROUND_TRIGGER_EVENT, self, self.looksAroundTexts[self.nextLooksAroundTrigger-1], 9)
				
		if self.nextWaitsTrigger < len(self.waitsTriggers):
			waitsTriggerPosition = self.waitsTriggers[self.nextWaitsTrigger]
			if self.distance(position, waitsTriggerPosition) < 0.8:
				self.nextWaitsTrigger += 1
				if self.waitsEnabled:
					viz.sendEvent(self.AVATAR_ON_WAITS_TRIGGER_EVENT, self, self.waitsTexts[self.nextWaitsTrigger-1], 1)
		
	def setAvatarState(self, state):
		self.vizAvatar.pauseActions()
		oldSpeed = self.speed
		self.vizAvatar.speed(1)
		self.vizAvatar.addAction(vizact.animation(state),1)
		vizact.ontimer2(self.vizAvatar.getDuration(state),0, self.ContinueAction, oldSpeed)
		
	def ContinueAction(self, speed):
		self.vizAvatar.resumeActions()
		self.vizAvatar.speed(speed)

	def checkPositionIsovist(self):
		position = [self.vizAvatar.getPosition()[0],self.vizAvatar.getPosition()[1],self.vizAvatar.getPosition()[2]]
		if self.nextIsovistTrigger < len(self.isovists.triggers)-1:
			isovistTriggerPosition = self.isovists.triggers[self.nextIsovistTrigger]
			if self.distance(position, isovistTriggerPosition) < 2.8:
				self.nextIsovistTrigger += 1
				self.OnIsovistTrigger()
				if self.isovistEnabled:
					viz.sendEvent(self.AVATAR_ON_ISOVIST_TRIGGER_EVENT, self, isovistTriggerPosition)

	def OnIsovistTrigger(self):
		if self.isovistsOn:
			self.isovistModels[self.nextIsovistTrigger-1].visible(viz.OFF)
			self.isovistModels[self.nextIsovistTrigger].visible(viz.ON)
	
	def ToggleIsovists(self):
		"""Bind this function for all avatars to a key to toggle isovists on and off."""
		if self.isovistsOn:
			self.isovistsOn = False
			self.isovistModels[self.nextIsovistTrigger].visible(viz.OFF)
			self.isovistModels[self.nextIsovistTrigger-1].visible(viz.OFF)
		else:
			self.isovistsOn = True
			self.isovistModels[self.nextIsovistTrigger].visible(viz.ON)
			
	def setSpeed(self, s):
		self.speed = s
		self.vizAvatar.speed(s)

	def __init__(self, cfg, dataSet, index, isovistHeight):
		viz.EventClass.__init__(self)
		
		#read json data file
		self.parsedDataSet = jsonParser.getAvatarsData(dataSet)[index]
		
		self.id = self.parsedDataSet["id"]
		self.movementSpaceId = -1
		self.speed = 1
		self.isovistsOn = True
		
		self.visibleObjectsIds = []
		self.linesToObjects = []
		self.visibleLinesIds = []
		self.linesFromTriggerToObjectIds = []
		self.linesFromTriggerToObject = []
		
		self.route = self.parsedDataSet["geometric_path"]
		
		#create Vizard-Avatar 
		self.vizAvatar = viz.addAvatar(cfg, pos=self.route[0])
		
		#Event ids
		self.AVATAR_ON_NARRATIVE_TRIGGER_EVENT = viz.getEventID('AvatarOnNarrativeTrigger' + str(self.id))
		self.AVATAR_ON_VISIBLE_TRIGGER_EVENT = viz.getEventID('AvatarOnVisibleTrigger' + str(self.id))
		self.AVATAR_ON_ASK_TRIGGER_EVENT = viz.getEventID('AvatarOnAskTrigger' + str(self.id))
		self.AVATAR_ON_FRUSTRATED_TRIGGER_EVENT = viz.getEventID('AvatarOnFrustratedTrigger' + str(self.id))
		self.AVATAR_ON_LOOKSAROUND_TRIGGER_EVENT = viz.getEventID('AvatarOnLooksAroundTrigger' + str(self.id))
		self.AVATAR_ON_WAITS_TRIGGER_EVENT = viz.getEventID('AvatarOnWaitsTrigger' + str(self.id))
		self.AVATAR_ON_ISOVIST_TRIGGER_EVENT = viz.getEventID('AvatarOnIsovistTrigger' + str(self.id))
		
		#Event enabled bools
		self.narrativeEnabled = False
		self.visibleEnabled = False
		self.askEnabled = False
		self.frustratedEnabled = False
		self.looksAroundEnabled = False
		self.waitsEnabled = False
		self.isovistEnabled = False
		
		#loading event datas
		self.narrative = self.parsedDataSet["narrative"]
		self.enteringTopologicalProduct = self.parsedDataSet["entering_topological_product"]
		self.visibleAtEntrance = self.parsedDataSet["visible_at_entrance"]
		self.ask = self.parsedDataSet["UserAsksForDirections"]
		self.frustrated = self.parsedDataSet["UserIsFrustrated"]
		self.looksAround = self.parsedDataSet["UserLooksAround"]
		self.wait = self.parsedDataSet["UserWaits"]
		
		#list of Isovist data, models, triggers and visible objects
		self.isovists = IsovistClass.Isovist(self.parsedDataSet["isovists"])
		self.isovistModels = ModelUtils.CreateIsovistModels(self.isovists, 0,0.8,0, 0.2, isovistHeight)
		
		#lists of triggers and objects related to the triggers
		self.narrativTriggers = []
		self.narrativTexts = []
		self.topologicalTriggers = []
		self.toplogicalObjects = []
		self.visibleTriggers = []
		self.visibleObjects = []
		self.askTriggers = []
		self.askTexts = []
		self.frustratedTriggers = []
		self.frustratedTexts = []
		self.looksAroundTriggers = []
		self.looksAroundTexts = []
		self.waitsTriggers = []
		self.waitsTexts = []
		
		#fill the lists above
		for a in self.ask:
			self.askTriggers.append(a["triggerValue"])
			self.askTexts.append(a["text"])
			
		for f in self.frustrated:
			self.frustratedTriggers.append(f["triggerValue"])
			self.frustratedTexts.append(f["text"])
			
		for l in self.looksAround:
			self.looksAroundTriggers.append(l["triggerValue"])
			self.looksAroundTexts.append(l["text"])
			
		for w in self.wait:
			self.waitsTriggers.append(w["triggerValue"])
			self.waitsTexts.append(w["text"])
		
		for n in self.narrative:
			self.narrativTriggers.append(n["triggerValue"])
			self.narrativTexts.append(n["text"])

		for v in self.visibleAtEntrance:
			self.visibleTriggers.append(v["triggerValue"])
			self.visibleObjects.append(v["products"])

		for t in self.enteringTopologicalProduct:
			self.topologicalTriggers.append(t["triggerValue"])
			self.toplogicalObjects.append(t["object"])
			
		#the indexes for current triggers
		self.nextNarrativeTrigger = 0
		self.nextTopologicalTrigger = 0
		self.nextVisibleTrigger = 0
		self.nextAskTrigger = 0
		self.nextFrustratedTrigger = 0
		self.nextLooksAroundTrigger = 0
		self.nextWaitsTrigger = 0
		self.nextIsovistTrigger = 0

		self.isovistTimer = vizact.ontimer(0, self.checkPositionIsovist)
		self.checkPositionTimer = vizact.ontimer(0, self.checkPositionEvents)
		