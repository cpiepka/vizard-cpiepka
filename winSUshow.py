﻿import ModelUtils
import DesignUtils
import CameraUtils
import AvatarUtils
import WindowsSetupUtils
import viz
import vizact

gulbenkian = DesignUtils.load_gulbenkian_no_roof()

viz.MainWindow.fov(60)
viz.setMultiSample(4)
viz.clearcolor(viz.SKYBLUE)
viz.go()

CameraUtils.SetFTNCam(gulbenkian)

avatars = AvatarUtils.CreateAvatars('dsim-json/Gulbenkian/IsovistNarrativeRoutes2.JSON', 1.0, 4.59999275208, 'vcc_male.cfg')
routeGraph = ModelUtils.create2dPolygons('dsim-json/Gulbenkian/Artefacts.json', 'Route Graph V3', 0.9,0,0, 0.2)
routeGraph.pop(len(routeGraph)-1)
routeGraph.pop(len(routeGraph)-1)
routeGraph.pop(len(routeGraph)-1)

#ModelUtils.AllVisible(routeGraph)
vizact.ontimer(0,ModelUtils.showMovementSpacesCurrent, routeGraph, avatars)

miniMap = WindowsSetupUtils.CreateMiniMap(gulbenkian)

WindowsSetupUtils.RenderIsovistsOnlyToWindows([miniMap], avatars)
WindowsSetupUtils.RenderOnlyToWindows(routeGraph, [viz.MainWindow])