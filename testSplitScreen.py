﻿import viz
import vizact
import WindowsSetupUtils

viz.setMultiSample(4)
viz.fov(60)
viz.go()

ground = viz.addChild('ground.osgb')
avatar = viz.addAvatar('vcc_male.cfg')
avatar.state(1)

#viz.MainView.move([0,10,-10])
#viz.lookAt([5,0,0])
viz.MainView.setPosition([1,2,-3])
viz.MainView.setEuler([0, 20, 0])

viz.clearcolor(viz.SKYBLUE)

def changeSecondView(window):
	mainPosition = viz.MainView.getPosition()
	window.getView().setPosition(mainPosition[0], mainPosition[1], mainPosition[2]-5)
	mainEuler = viz.MainView.getEuler()
	window.getView().setEuler(mainEuler[0], mainEuler[1], mainEuler[2])

windows = WindowsSetupUtils.SplitScreenVert()

vizact.ontimer(0, changeSecondView, windows[0])
