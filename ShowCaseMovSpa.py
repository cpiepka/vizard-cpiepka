﻿import ModelUtils
import DesignUtils
import CameraUtils
import AvatarUtils
import viz
import vizact

gulbenkian = DesignUtils.load_gulbenkian_no_roof()

viz.MainWindow.fov(60)
viz.setMultiSample(4)
viz.clearcolor(viz.SKYBLUE)
viz.go()

CameraUtils.SetFTNCam(gulbenkian)

dsObjects = ModelUtils.createDesignObjects('dsim-json\Gulbenkian\Gulbenkian.ifc.JSON')

avatars = AvatarUtils.CreateAvatars('dsim-json/Gulbenkian/IsovistNarrativeRoutes2.JSON', 1.0, 4.59999275208, 'vcc_male.cfg')
ModelUtils.toggleIsovists(avatars)

routeGraph = ModelUtils.create2dPolygons('dsim-json/Gulbenkian/Artefacts.json', 'Route Graph V3', 0.9,0,0, 0.2)
routeGraph.pop(len(routeGraph)-1)
routeGraph.pop(len(routeGraph)-1)
routeGraph.pop(len(routeGraph)-1)

movementSpaceTimer0 = vizact.ontimer(0, ModelUtils.showMovementSpacesCurrent, routeGraph, avatars)
movementSpaceTimer1 = vizact.ontimer(0, ModelUtils.showMovementSpacesTrace, routeGraph, avatars)
movementSpaceTimer0.setEnabled(False)

vizact.onkeydown('m', ModelUtils.toggleMovementSpaceMode, movementSpaceTimer0, movementSpaceTimer1)