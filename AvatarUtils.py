﻿import AvatarClass
import jsonParser
import vizact

avatars = []

def newAvatarWithRoute(cfg, dataSet, index, scale, isovistHeight):
	newAvatar = AvatarClass.Avatar(cfg, dataSet, index, isovistHeight)
	newAvatar.vizAvatar.speed(speed)
	newAvatar.vizAvatar.setScale(scale, scale, scale)
	
	avatars.append(newAvatar)
	goRoute(newAvatar)

def goRoute(avatar):
    for r in avatar.route:
        walk = vizact.walkTo([r[0],r[1],r[2]])
        avatar.vizAvatar.addAction(walk)


def CreateAvatars(json, scale, isovistHeight, cfg):
	'''Create all Avatars that are given in a json file.
	This is the function to call in a script.
	@param json: The json file, that contains the information about the avatars.
	@type json: A .json file'''
	i = 0
	dataLength = len(jsonParser.openFile(json))
	while i < dataLength:
		newAvatarWithRoute(cfg, json, i, scale, isovistHeight)
		i += 1
	return avatars

#speed-settings
speed = 1
def speedUp(ava):
    global speed
    if speed < 3:
        speed = speed + 1
    for a in ava:
        a.setSpeed(speed)
        
def speedDown(ava):
    global speed
    if speed > 1:
        speed = speed -1
    for a in ava:
        a.setSpeed(speed)
        
def speedPause(ava):
    global speed
    speed =0
    for a in ava:
        a.setSpeed(speed)
