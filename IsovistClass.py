﻿class Isovist():
	def __init__(self, dataSet):
		self.ids = []
		self.triggers = []
		self.visibleProducts = []
		self.contours = []
		
		i = 0
		while i < len(dataSet):
			self.ids.append(dataSet[i]["eventId"])
			self.triggers.append(dataSet[i]["trigger"])
			self.visibleProducts.append(dataSet[i]["visibleProducts"])
			for c in dataSet[i]["contours"]:
				self.contours.append(c)
			i += 1