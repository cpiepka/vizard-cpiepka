﻿import viz
import vizact
import WindowsSetupUtils

viz.setMultiSample(4)
viz.fov(60)
viz.go()

ground = viz.addChild('ground.osgb')
avatar = viz.addAvatar('vcc_male.cfg')
avatar.state(1)

viz.MainView.move([0,10,-20])
viz.lookAt([0,0,0])
viz.clearcolor(viz.SKYBLUE)

	
def switchViews(window00, window11):
	view00 = window00.getView()
	view11 = window11.getView()
	window00.setView(view11)
	window11.setView(view00)

windows = WindowsSetupUtils.FourWindowsOnTop()
vizact.onkeydown('s', switchViews, viz.MainWindow, windows[3])
