﻿import viz
import vizact
import vizcam
import vizinfo
import vizshape
import utils
import data_scripts
import jsonReader
import ModelClass
import time
import thread
import threading


viz.MainWindow.fov(60)
viz.setMultiSample(4)
viz.clearcolor(viz.SKYBLUE)
viz.go()

vizcam.AUTO_REGISTER = 0

info = vizinfo.InfoPanel("")
info.visible(viz.OFF)

info2 = vizinfo.InfoPanel("", viz.ALIGN_CENTER_BOTTOM, icon=False)
info2.visible(viz.OFF)

hospital = data_scripts.load_gulbenkian_no_roof()
#hospital = data_scripts.load_normain('C:\Users\Christoph\Dropbox\Vizard\dsim-json\Normain-Interchange\Norman-Interchange\July 5-2013 Changes-1st-floor-no-roof.OSGB')
#hospital = data_scripts.load_normain('C:\Users\Christoph\Dropbox\Vizard\dsim-json\Normain-Interchange\Norman-Interchange\July 5-2013 Changes-ground-floor-no-roof.OSGB')

scale = data_scripts.get_parkland_scale()

BirdEyeWindow = viz.addWindow()
BirdEyeWindow.fov(60)
BirdEyeWindow.visible(0, viz.SCREEN)
BirdEyeWindow.setSize([0.4, 0.4])
print(BirdEyeWindow.getPosition())
BirdEyeWindow.setPosition([0.6, 1])
BirdEyeView = viz.addView()
BirdEyeWindow.setView(BirdEyeView)
BirdEyeView.setPosition([45, 64, 27])
#BirdEyeView.setPosition([45,200,27])
BirdEyeView.setEuler([0, 90, 0])




#Controlling speed
vizact.onkeydown(viz.KEY_KP_ADD, utils.speedUp, utils.avatars)
vizact.onkeydown(viz.KEY_KP_SUBTRACT, utils.speedDown, utils.avatars)
vizact.onkeydown('p', utils.speedPause, utils.avatars)

#lists of models
emptySpaces = [] # Empty Spaces V2
functionalSpaces = [] # Functional Spaces
operationalSpaces = [] # Operational Spaces
routeGraph = [] # Route Graph V3
standardQvGraph = [] # Standard QvGraph
sunLight = [] # Sun Light
wayfindingContinuity = [] # Wayfinding Continuity
designModels = [] # ds_products


def CreateAvatars(json):
    i = 0
    dataLength = len(jsonReader.openFile(json))
    while i < dataLength:
        utils.newAvatarWithRouteV2('vcc_male.cfg', json, i)
        i += 1


def ModelById(models, id):
    for m in models:
        if m.id == id:
            return m


def alphaUp(model):
    pass





def MakeObjectsVisible(avatar, index):
    if len(avatar) > index:
        if avatar[index].isovistTriggered == True:
            ids = avatar[index].isovists.visibleProducts[avatar[index].nextIsovistTriggerIndex - 1]
            for m in designModels:
                #TODO: fade models out, that are not longer visible
                #m.model.visible(viz.OFF)
                pass
            for id in ids:
                #ModelById(designModels, id).model.visible(viz.ON)
                #fadeModelIn(ModelById(designModels, id).model)
                model = ModelById(designModels, id).model
                fadeIn = thread.start_new_thread(utils.fadeModelIn, (model,))
                #fadeInThread = threading.Thread()


def SetAvatarNarratives(avatar, index):
    if len(avatar) > index:
        if avatar[index].narrativeTriggered == True:
            info.visible(viz.ON)
            text = avatar[index].narrativTexts[avatar[index].nextNarrativeTriggerIndex - 1]
            splitted = text.split()
            i = 15
            while i < len(splitted):
                splitted.insert(i, '\n')
                i += 15
            text = ' '.join(splitted)
            info.setText(text)
            MakeObjectsVisible(avatar, index)

        if avatar[index].askTriggered == True:
            info2.visible(viz.ON)
            text = avatar[index].askTexts[avatar[index].nextAskTrigger - 1]
            info2.setText(text)
            vizact.ontimer2(avatar[index].vizAvatar.getDuration(14), 0, info2.visible, viz.OFF)

        if avatar[index].frustratedTriggered == True:
            info2.visible(viz.ON)
            text = avatar[index].frustratedTexts[avatar[index].nextFrustratedTrigger - 1]
            info2.setText(text)
            vizact.ontimer2(avatar[index].vizAvatar.getDuration(3), 0, info2.visible, viz.OFF)

        if avatar[index].looksAroundTriggered == True:
            info2.visible(viz.ON)
            text = avatar[index].looksAroundTexts[avatar[index].nextLooksAroundTrigger - 1]
            info2.setText(text)
            vizact.ontimer2(avatar[index].vizAvatar.getDuration(9), 0, info2.visible, viz.OFF)

        if avatar[index].waitsTriggered == True:
            info2.visible(viz.ON)
            text = avatar[index].waitsTexts[avatar[index].nextWaitsTrigger - 1]
            info2.setText(text)
            vizact.ontimer2(avatar[index].vizAvatar.getDuration(1), 0, info2.visible, viz.OFF)

#			self.askTriggered = False
#			self.frustratedTriggered = False
#			self.looksAroundTriggered = False
#			self.waitsTriggered = False

timer = vizact.EventFunction

def LinkNarrativeCam(avatar, index):
    global timer
    info.visible(viz.OFF)
    utils.linkCam(avatar, index)
    timer.remove()
    timer = vizact.ontimer(0, SetAvatarNarratives, avatar, index)

def OverviewCam(location):
    global timer
    timer.remove()
    info.visible(viz.OFF)
    utils.overviewCam(location)


CreateAvatars('dsim-json/Gulbenkian/IsovistNarrativeRoutes2.json')
#CreateAvatars('dsim-json/Normain-Interchange/IsovistNarrativeRoutes-1.json')
#CreateAvatars('dsim-json/Normain-Interchange/NarrativeRoutes.json')



a = 0			
while a < len(utils.avatars):
	vizact.ontimer(0, MakeObjectsVisible,utils.avatars,a)
	a += 1

#data = jsonReader.openFile('JsonArtefacts.json')
#functionalSpaces = data["Functional Spaces"]
#testModel = ModelClass.Model(functionalSpaces[0]["guid"])
#testModel.CreateModel(functionalSpaces[0]["representation"]["footprint"]["contours"][0], functionalSpaces[0]["representation"]["height"])
#print(functionalSpaces[0]["representation"]["footprint"]["contours"])
#testModel.model.color(0.5,0,0)

#Controll the cam
vizact.onkeydown('1', LinkNarrativeCam, utils.avatars, 0)
vizact.onkeydown('2', LinkNarrativeCam, utils.avatars, 1)
vizact.onkeydown('3', LinkNarrativeCam, utils.avatars, 2)
vizact.onkeydown('4', LinkNarrativeCam, utils.avatars, 3)
vizact.onkeydown('5', LinkNarrativeCam, utils.avatars, 4)
vizact.onkeydown('6', LinkNarrativeCam, utils.avatars, 5)
vizact.onkeydown('7', LinkNarrativeCam, utils.avatars, 6)
vizact.onkeydown('8', LinkNarrativeCam, utils.avatars, 7)
vizact.onkeydown('9', LinkNarrativeCam, utils.avatars, 8)
vizact.onkeydown('0', LinkNarrativeCam, utils.avatars, 9)
vizact.onkeydown(viz.KEY_BACKSPACE, OverviewCam, hospital)


def NoIsovistsOnMiniMap():
	for a in utils.avatars:
		for m in a.isovistModels:
			m.renderToAllWindowsExcept([BirdEyeWindow])

routeLines = []
for a in utils.avatars:	
	viz.startLayer(viz.LINE_STRIP)
	viz.vertexColor(viz.RED)
	lines = viz.endLayer(parent=viz.ORTHO,scene=BirdEyeWindow)
	lines.dynamic() 
	routeLines.append(lines)

def DrawPathToMiniMap():
	for a in utils.avatars:
		x,y,z = BirdEyeWindow.worldToScreen(a.vizAvatar.getPosition(),mode=viz.WINDOW_PIXELS)
		#x,y,z = a.vizAvatar.getPosition()
		#print(x)
		lx,ly,lz = lines.getVertex(-1)
		xdif = abs(x-lx)
		ydif = abs(y-ly)
#		print(xdif)
#		print(ydif)
#		if ydif > 0.1 or xdif > 0.1:
#			return
		if x != lx or y != ly:
			routeLines[a.id].addVertex([x,y,0.0])

def AllInvisible(models):
	for m in models:
		m.model.visible(viz.OFF)

def AllVisible(models):
	for m in models:
		m.model.visible(viz.ON)

allDesignsVisible = False
allFunctionalsVisible = False
allOperationalsVisible = False
isovistsOn = True


#def ToggleIsiovists():

def SwitchFunctionalBool():
	global allFunctionalsVisible
	if allFunctionalsVisible:
		allFunctionalsVisible = False
	else:
		allFunctionalsVisible = True
	#print(allFunctionalsVisible)


def ToggleVisibility(models, bool):
	print(bool)
	if not allFunctionalsVisible:
		AllVisible(models)
	else:
		AllInvisible(models)
	SwitchFunctionalBool()

#emptySpaces = CreateArtefactModels('JsonArtefacts.json', 'Empty Spaces V2', 0,0,0.5)	
functionalSpaces = utils.CreateModels('dsim-json/Gulbenkian/Artefacts.json', 'Functional Spaces', 0.5,0,0, 0.3)	
#functionalSpaces = CreateArtefactModels('dsim-json/Normain-Interchange/Artefacts.json', 'Functional Spaces', 0.5,0,0)	


#operationalSpaces = CreateArtefactModels('JsonArtefacts.json', 'Operational Spaces', 0,0.5,0)	
#routeGraph = CreateArtefactModels('JsonArtefacts.json', 'Route Graph V3', 0.5,0.5,0)	
#standardQvGraph = CreateArtefactModels('JsonArtefacts.json', 'Standard QvGraph', 0.5,0,0.5)	
#sunLight = CreateArtefactModels('JsonArtefacts.json', 'Sun Light', 0,0.5,0.5)	
#wayfindingContinuity = CreateArtefactModels('JsonArtefacts.json', 'Wayfinding Continuity', 0.5,0.5,0.5)	

designModels = utils.CreateModels('dsim-json/Gulbenkian/Design.json', 'ds_products', 0.8,0,0, 0.3)
#designModels = CreateDesignModels('dsim-json/Normain-Interchange/Design.json', 'ds_products', 0.8,0,0)

#vizact.onkeydown('f', SwitchFunctionalBool)
vizact.onkeydown('f', ToggleVisibility, functionalSpaces, allFunctionalsVisible)
#vizact.onkeydown('g', )

#NoIsovistsOnMiniMap()
vizact.ontimer(0, DrawPathToMiniMap)

#TODO: find way to do this right
#for a in utils.avatars:
#	a.vizAvatar.pauseActions()
#
#def Start():
#	for a in utils.avatars:
#		a.vizAvatar.resumeActions()
#
#vizact.onkeydown('s',Start)