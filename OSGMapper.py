﻿import math

def distance3d(point0, point1):
    try:
        return math.sqrt((point0[0]-point1[0]) * (point0[0]-point1[0])
            +(point0[1]-point1[1]) * (point0[1]-point1[1])
            +(point0[2]-point1[2]) * (point0[2]-point1[2]))
    except:
        print 'TypeError: Arguments have to be points (x,y,z)'
        
def mapObjectsToOSGNodes(objects, osg, scale):
	retVal = {}
	osgNodes = []
	for name in osg.getNodeNames():
		child = osg.getChild(name)
		if child.id > -1:
			osgNodes.append(child)

	for o in objects:
		sphereCentroid = o.bsCentroid
		if sphereCentroid is None:
			continue

		nearestDistance = float('inf')
		nearestNode = None
		for node in osgNodes:
			nodeCenter = node.getBoundingSphere().getCenter() 
			scaledNodeCenter = (nodeCenter[0]*scale, nodeCenter[1]*scale, nodeCenter[2]*scale)
			distance = distance3d(sphereCentroid, scaledNodeCenter)
			if distance < nearestDistance:
				nearestDistance = distance
				nearestNode = node
		retVal[o.guid] = nearestNode
	return retVal

