﻿import RouteGraph
import viz
import data_scripts
import utils

viz.MainWindow.fov(60)
viz.setMultiSample(4)
viz.clearcolor(viz.SKYBLUE)
viz.go()


gulbenkian = data_scripts.load_gulbenkian_no_roof()
scl = data_scripts.get_gulbenkian_scale()

utils.SetFTNCam(gulbenkian)

node1 = RouteGraph.ConnectionNode([1105.42114*scl, 1, 275.59055*scl], 1)
node2 = RouteGraph.ConnectionNode([1701.73181*scl, 1, 288.62943*scl], 2)
node3 = RouteGraph.ConnectionNode([2193.89038*scl, 1, 628.29761*scl], 3)
node4 = RouteGraph.ConnectionNode([773.46332*scl, 1, 639.97821*scl], 4)
node5 = RouteGraph.ConnectionNode([1105.42114*scl, 1, 275.59055*scl], 1)
node6 = RouteGraph.ConnectionNode([1130.83655*scl, 1, -12.35184*scl], 6)

rNode1 = RouteGraph.RoomNode([1461.29297*scl, 1, 502.71094*scl], 1, [node1,node2,node3, node4])
rNode2 = RouteGraph.RoomNode([1132.34888*scl, 1, 147.55505*scl], 2, [node5, node6])

rg = RouteGraph.RouteGraph([rNode1, rNode2])
