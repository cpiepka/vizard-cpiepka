﻿import utils

visiblePolygons = []
invisiblePolygons = []
currentPolygons = []
visitedPolygons = []
def setVisitedMovementSpaces(polygonModels):
	for a in utils.avatars:
		pos = a.vizAvatar.getPosition()
		wasInside = False
		for p in polygonModels:
			if p.polygon.isInside(pos[0], pos[2]):
				a.movementSpaceId = p.id
				wasInside = True
				if len(visitedPolygons) <= a.id:
					modelToAdd = [p]
					visitedPolygons.append(modelToAdd)
				elif visitedPolygons[a.id].count(p) == 0:
					visitedPolygons[a.id].append(p)
		if not wasInside:
			a.movementSpaceId = None
				
def showMovementSpacesCurrent():
	for list in visitedPolygons:
		for model in list:
			model.model.visible(False)
	
	for a in utils.avatars:
		if a.movementSpaceId != None:
			visitedPolygons[a.id][len(visitedPolygons[a.id])-1].model.visible(True)
		
		
		
def showMovementSpacesTrace():
    for p in visiblePolygons:
        p.model.visible(viz.ON)