﻿import viz
import ModelClass

windows = []
def SplitScreenVert():
	viz.MainWindow.setSize([0.5, 1])
	#viz.MainWindow.setPosition([0.5, 1])
	
	windows = []
	window = viz.addWindow()
	window.fov(viz.MainWindow.getVerticalFOV())
	window.setSize([0.5, 1])
	window.setPosition([0.5, 1])
	view = viz.addView()
	window.setView(view)
	view.setPosition(viz.MainView.getPosition())
	view.setEuler(viz.MainView.getEuler())
	window.clearcolor(viz.MainWindow.getClearColor())
	windows.append(window)
	return windows
	
def SplitScreenHor():
	windows = []
	window = viz.addWindow()
	window.fov(viz.MainWindow.getVerticalFOV())
	window.setSize([1, 0.5])
	window.setPosition([0, 1])
	view = viz.addView()
	window.setView(view)
	view.setPosition(viz.MainView.getPosition())
	view.setEuler(viz.MainView.getEuler())
	window.clearcolor(viz.MainWindow.getClearColor())
	windows.append(window)
	return windows
	
def FourWindowsOnTop():
	windows = []
	
	windowsize = [0.25,0.35]
	
	window0 = viz.addWindow()
	window0.fov(viz.MainWindow.getVerticalFOV())
	window0.setSize(windowsize)
	window0.setPosition([0, 1])
	view0 = viz.addView()
	window0.setView(view0)
	view0.setPosition(viz.MainView.getPosition())
	view0.setEuler(viz.MainView.getEuler())
	window0.clearcolor(viz.MainWindow.getClearColor())
	windows.append(window0)
	
	window1 = viz.addWindow()
	window1.fov(viz.MainWindow.getVerticalFOV())
	window1.setSize(windowsize)
	window1.setPosition([0.25, 1])
	view1 = viz.addView()
	window1.setView(view1)
	view1.setPosition(viz.MainView.getPosition())
	view1.setEuler(viz.MainView.getEuler())
	window1.clearcolor(viz.MainWindow.getClearColor())
	windows.append(window1)
	
	window2 = viz.addWindow()
	window2.fov(viz.MainWindow.getVerticalFOV())
	window2.setSize(windowsize)
	window2.setPosition([0.5, 1])
	view2 = viz.addView()
	window2.setView(view2)
	view2.setPosition(viz.MainView.getPosition())
	view2.setEuler(viz.MainView.getEuler())
	window2.clearcolor(viz.MainWindow.getClearColor())
	windows.append(window2)
	
	window3 = viz.addWindow()
	window3.fov(viz.MainWindow.getVerticalFOV())
	window3.setSize(windowsize)
	window3.setPosition([0.75, 1])
	view3 = viz.addView()
	window3.setView(view3)
	view3.setPosition(viz.MainView.getPosition())
	view3.setEuler(viz.MainView.getEuler())
	window3.clearcolor(viz.MainWindow.getClearColor())
	windows.append(window3)
	
	return windows
	
def TwoWindowsOnSide():
	windows = []
	
	windowsize = [0.4,0.5]
	
	window0 = viz.addWindow()
	window0.fov(viz.MainWindow.getVerticalFOV())
	window0.setSize(windowsize)
	window0.setPosition([0.6, 1])
	view0 = viz.addView()
	window0.setView(view0)
	view0.setPosition(viz.MainView.getPosition())
	view0.setEuler(viz.MainView.getEuler())
	window0.clearcolor(viz.MainWindow.getClearColor())
	windows.append(window0)
	
	window1 = viz.addWindow()
	window1.fov(viz.MainWindow.getVerticalFOV())
	window1.setSize(windowsize)
	window1.setPosition([0.6, 0.5])
	view1 = viz.addView()
	window1.setView(view1)
	view1.setPosition(viz.MainView.getPosition())
	view1.setEuler(viz.MainView.getEuler())
	window1.clearcolor(viz.MainWindow.getClearColor())
	windows.append(window1)
	
	return windows
	
def ThreeWindowsOnSide():
	windows = []
	
	windowsize = [0.33,0.33333]
	
	window0 = viz.addWindow()
	window0.fov(viz.MainWindow.getVerticalFOV())
	window0.setSize(windowsize)
	window0.setPosition([0.67, 1])
	view0 = viz.addView()
	window0.setView(view0)
	view0.setPosition(viz.MainView.getPosition())
	view0.setEuler(viz.MainView.getEuler())
	window0.clearcolor(viz.MainWindow.getClearColor())
	windows.append(window0)
	
	window1 = viz.addWindow()
	window1.fov(viz.MainWindow.getVerticalFOV())
	window1.setSize(windowsize)
	window1.setPosition([0.67, 0.66666])
	view1 = viz.addView()
	window1.setView(view1)
	view1.setPosition(viz.MainView.getPosition())
	view1.setEuler(viz.MainView.getEuler())
	window1.clearcolor(viz.MainWindow.getClearColor())
	windows.append(window1)
	
	window2 = viz.addWindow()
	window2.fov(viz.MainWindow.getVerticalFOV())
	window2.setSize(windowsize)
	window2.setPosition([0.67, 0.33333])
	view2 = viz.addView()
	window2.setView(view2)
	view2.setPosition(viz.MainView.getPosition())
	view2.setEuler(viz.MainView.getEuler())
	window2.clearcolor(viz.MainWindow.getClearColor())
	windows.append(window2)
	
	return windows
	
def CreateMiniMap(gallery):
	bs = gallery.getBoundingSphere()
	c = bs.getCenter()
	r = bs.getRadius()
	viewPos = [c[0], c[1]+r, c[2]]
	window = viz.addWindow()
	window.fov(60)
	window.visible(0, viz.SCREEN)
	window.setSize([0.4, 0.4])
	window.setPosition([0.6, 1])
	view = viz.addView()
	window.setView(view)
	view.setPosition(viewPos)
	view.setEuler([0, 90, 0])
	return window

	
def changeSecondView(window, avatar, index):
	newLink = viz.link(avatar[index].vizAvatar, window.getView())
	avatarScale = avatar[index].vizAvatar.getScale()
	newLink.preTrans([0*avatarScale[0],2.2*avatarScale[1],-5*avatarScale[2]])
	newLink.preEuler([0,10,0])
	
#	mainPosition = viz.MainView.getPosition()
#	window.getView().setPosition(mainPosition[0], mainPosition[1], mainPosition[2])
#	mainEuler = viz.MainView.getEuler()
#	window.getView().setEuler(mainEuler[0], mainEuler[1], mainEuler[2])

def deleteWindows(windows):
	for w in windows:
		w.remove()
	viz.MainWindow.setSize([1,1])
	return []
	
def switchViews(window0, window1):
	view0 = window0.getView()
	view1 = window1.getView()
	window0.setView(view1)
	window1.setView(view0)
	
def RenderToAllWindowsExcept(models ,windows):
	for m in models:
		if isinstance(m, ModelClass.Model):
			m.model.renderToAllWindowsExcept(windows)
		elif isinstance(m, DSObjectClass.DSObject) and not m.polygon is None:
			m.polygon[0].renderToAllWindowsExcept(windows)

def RenderOnlyToWindows(models, windows):
	for m in models:
		if isinstance(m, ModelClass.Model):
			m.model.renderOnlyToWindows(windows)
		elif isinstance(m, DSObjectClass.DSObject) and not m.polygon is None:
			m.polygon[0].renderOnlyToWindows(windows)
		
def RenderIsovistsOnlyToWindows(windows, avatars):
	for a in avatars:
		for m in a.isovistModels:
			m.renderOnlyToWindows(windows)
