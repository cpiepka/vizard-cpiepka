﻿import threading
import ModelUtils

class FadeInThread(threading.Thread):
	
	def __init__(self, model):
		threading.Thread.__init__(self)
		self.model = model
		
	def run(self):
		ModelUtils.fadeModelIn(self.model)
		
class FadeOutThread(threading.Thread):
	
	def __init__(self, model):
		threading.Thread.__init__(self)
		self.model = model
		
	def run(self):
		ModelUtils.fadeModelOut(self.model)