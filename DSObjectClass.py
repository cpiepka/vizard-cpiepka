﻿import jsonParser
import ModelCreater

class DSObject():
	def __init__(self, objectData):
		self.guid = objectData['id']
		self.description = objectData['desc']
		self.ds_class_type = objectData['ds_class']
		self.ifc_class_type = objectData['ifc_class']
		self.name = objectData['name']
		
		if objectData.has_key('polygonData'):
			self.createPolygon(objectData['polygonData']['contour'], objectData['polygonData']['holes'],
				objectData['polygonData']['height'], objectData['polygonData']['elevation'])
		else:
			self.polygon = None
			
		if objectData.has_key('boundingSphere'):
			'set bs'
			self.bsRadius = objectData['boundingSphere']['radius']
			self.bsCentroid = objectData['boundingSphere']['centroid']
		else:
			self.bsRadius = None
			self.bsCentroid = None
		
		
	def createPolygon(self, contour, holes, height, elevation):
		self.polygon = ModelCreater.Create3DPolygon(contour, holes, height, elevation)