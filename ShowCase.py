﻿import viz
import vizact
import vizcam
import vizinfo
import data_scripts
import utils
import AvatarClass
import math
from ZoomEnum import Zoom
import ModelCreater
import WindowsSetupUtils

viz.MainWindow.fov(60)
viz.setMultiSample(4)
viz.clearcolor(viz.SKYBLUE)
viz.go()

vizcam.AUTO_REGISTER = 0

gulbenkian = data_scripts.load_gulbenkian_no_roof()

utils.SetFTNCam(gulbenkian)

#info panel for the narratives
info = vizinfo.InfoPanel("")
info.visible(viz.OFF)

#info panel for the userstates
info2 = vizinfo.InfoPanel("", viz.ALIGN_CENTER_BOTTOM, icon=False)
info2.visible(viz.OFF)

linkedAvatarId = -1

#lists of models
emptySpaces = [] # Empty Spaces V2
functionalSpaces = [] # Functional Spaces
operationalSpaces = [] # Operational Spaces
routeGraph = [] # Route Graph V3
standardQvGraph = [] # Standard QvGraph
sunLight = [] # Sun Light
wayfindingContinuity = [] # Wayfinding Continuity
designModels = [] # ds_products

windows = []

#Avatars
utils.CreateAvatars('dsim-json/Gulbenkian/IsovistNarrativeRoutes2.JSON', 1.0, 4.59999275208) #avatars have isovists

#models
designModels = utils.CreateModels('dsim-json/Gulbenkian/Design.json', 'ds_products', 0,0.1,0.8, 0.2)
functionalSpaces = utils.CreateModels('dsim-json/Gulbenkian/Artefacts.json', 'Functional Spaces', 0.5,0,0, 0.2)
operationalSpaces = utils.CreateModels('dsim-json/Gulbenkian/Artefacts.json', 'Operational Spaces', 0,0.5,0, 0.2)
routeGraph = utils.CreateModels('dsim-json/Gulbenkian/Artefacts.json', 'Route Graph V3', 0.5,0.5,0, 0.2)
#wayfindingContinuity = utils.CreateModels('dsim-json/Gulbenkian/Artefacts.json', 'Wayfinding Continuity', 0.5,0.5,0.5, 0.2)	

#handling the isovist-event -> fading visible models in
def onIsovist(avatar):
    utils.MakeObjectsVisible(avatar, designModels)
    utils.MakeObjectsInvisible(avatar, designModels)
    utils.LinesToVisibleObjects(avatar, designModels, 0,0,0.7, 0.2)

#handling the narrative-event -> set the infotext
def onNarrativ(avatarId, text):
    if linkedAvatarId == avatarId:
        info.visible(viz.ON)
        info.setText(utils.formatText(text, 8))
        
#handling the userstates -> set the infotext
def onUserState(avatar, text, state):
    avatar.setAvatarState(state)
    if linkedAvatarId == avatar.id:
        info2.visible(viz.ON)
        info2.setText(text)
        vizact.ontimer2(avatar.vizAvatar.getDuration(state), 0, info2.visible, viz.OFF)
        
#enable events
for a in utils.avatars:
    a.enableEvent('onIsovist', onIsovist) #isovist-event
    a.enableEvent('onNarrative', onNarrativ) #narrative-event
    a.enableEvent('onAsk', onUserState) #ask-event
    a.enableEvent('onWaits', onUserState) #wait-event
    a.enableEvent('onFrustrated', onUserState) #frustrated-event
    a.enableEvent('onLooksAround', onUserState) #looks-around-event

screenSplited = False
#different views
def LinkNarrativeCam(avatar, index):
	global linkedAvatarId
	linkedAvatarId = avatar[index].id
	info.visible(viz.OFF)
	info2.visible(viz.OFF)
	utils.linkCamBehind(avatar, index)
	if screenSplited:
		WindowsSetupUtils.changeSecondView(windows[0], avatar, index)

def OverviewCam(location):
    global linkedAvatarId
    linkedAvatarId = -1
    info.visible(viz.OFF)
    info2.visible(viz.OFF)
    utils.overviewCam(location)
    
def toggleIsovists():
    for a in utils.avatars:
        a.ToggleIsovists()

allFuncVisible = False
def toggleFunctionalSpaces():
	global allFuncVisible
	if not allFuncVisible:
		allFuncVisible = True
		utils.AllVisible(functionalSpaces)
	else:
		allFuncVisible = False
		utils.AllInvisible(functionalSpaces)
		
allOperationVisible = False
def toggleOperationalSpaces():
	global allOperationVisible
	if not allOperationVisible:
		allOperationVisible = True
		utils.AllVisible(operationalSpaces)
	else:
		allOperationVisible = False
		utils.AllInvisible(operationalSpaces)
		
def toggleShowLines():
	showLines = not showLines
	
def splitScreen():
	global screenSplited
	global windows
	if not screenSplited:
		windows = WindowsSetupUtils.SplitScreenVert()
		screenSplited = True
		if linkedAvatarId != -1:
			WindowsSetupUtils.changeSecondView(windows[0], utils.avatars, linkedAvatarId)
			
	
def standardScreen():
	global screenSplited
	global windows
	windows = WindowsSetupUtils.deleteAllWindows(windows)
	screenSplited = False
	
highlightingEnabled = True
def toggleHighlighting():
	global highlightingEnabled
	if highlightingEnabled:
		highlightingEnabled = not highlightingEnabled
		for a in utils.avatars:
			a.disableEvent('onIsovist')
			utils.fadeAllModelsOut(designModels)
			utils.fadeAllLinesOut(a.linesToObjects)
	else:
		highlightingEnabled = not highlightingEnabled
		for a in utils.avatars:
			a.enableEvent('onIsovist', onIsovist)
			utils.fadeAllLinesIn(a.linesToObjects)
			utils.FadeInVisibleObjects(a, designModels)


#bind keys
#Controlling models
vizact.onkeydown('i', toggleIsovists)# toggle isovists on and off

#Controlling speed
vizact.onkeydown(viz.KEY_KP_ADD, utils.speedUp, utils.avatars)
vizact.onkeydown(viz.KEY_KP_SUBTRACT, utils.speedDown, utils.avatars)
vizact.onkeydown('p', utils.speedPause, utils.avatars)

#Controll events
vizact.onkeydown('y', toggleHighlighting)

#Controll the cam
vizact.onkeydown('1', LinkNarrativeCam, utils.avatars, 0)
vizact.onkeydown('2', LinkNarrativeCam, utils.avatars, 1)
vizact.onkeydown('3', LinkNarrativeCam, utils.avatars, 2)
vizact.onkeydown('4', LinkNarrativeCam, utils.avatars, 3)
vizact.onkeydown('5', LinkNarrativeCam, utils.avatars, 4)
vizact.onkeydown('6', LinkNarrativeCam, utils.avatars, 5)
vizact.onkeydown('7', LinkNarrativeCam, utils.avatars, 6)
vizact.onkeydown('8', LinkNarrativeCam, utils.avatars, 7)
vizact.onkeydown('9', LinkNarrativeCam, utils.avatars, 8)
vizact.onkeydown('0', LinkNarrativeCam, utils.avatars, 9)
vizact.onkeydown(viz.KEY_BACKSPACE, OverviewCam, gulbenkian)

vizact.onkeydown('s', splitScreen)
vizact.onkeydown('a', standardScreen)

#Artefacts
vizact.onkeydown('f', toggleFunctionalSpaces)
vizact.onkeydown('o', toggleOperationalSpaces)

#highlighting