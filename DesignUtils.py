﻿import viz

def scaleDesign(gallery, x,y,z,width):
    bb = gallery.getBoundingBox()

    origWidth  =  bb.getWidth()
    if origWidth > 0:
        scaleFactor = width/origWidth
    else:
        scaleFactor = 1

    gallery.setScale(scaleFactor,scaleFactor,scaleFactor)
    gallery.setPosition(x,y,z)

def get_parkland_scale():
	return 322.6725/7.82542610168
	
def load_gulbenkian():
	gulbenkian = viz.addChild('dsim-json/Gulbenkian/Gulbenkian_Complete_Floorplan_withOpenings_withSlabs_c_v2_ARCHICAD_16.OSGB')
	scaleDesign(gulbenkian,0, 0, 0, 106.067)
	return gulbenkian
	
def load_gulbenkian_no_roof():
	gulbenkian = viz.addChild('dsim-json/Gulbenkian/Gulbenkian_Complete_Floorplan_withOpenings_withSlabs_No_Roof_c_v2_ARCHICAD_13.OSGB')
	scaleDesign(gulbenkian,0, 0, 0, 106.067)
	return gulbenkian
	
def get_gulbenkian_scale():
	return 0.0253999550291
	
def load_normain(path):
	normain = viz.addChild(path)
	scaleDesign(normain, 0,0,0, 569.041)
	return normain
	
	
def load_Trabajador():
	trab = viz.addChild('dsim-json/Trabajador/Trabajador.OSGB')
	scaleDesign(trab,0, 0, 0, 343.156)
	return trab
	
	
def load_Parkland():
	gallery = viz.addChild('dsim-json/Parkland/Parkland.OSGB')
	scaleDesign(gallery,0,0,0, 306.108)
	return gallery

def load_unknownDesign(designFile, scaleFactor):
	gallery = viz.addChild(designFile)
	scaleDesign(gallery,0,0,0, scaleFactor)
	return gallery
