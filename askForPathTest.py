﻿import DesignUtils
import CameraUtils
import AvatarUtils
import ModelUtils
import TextFormatter
import WindowsSetupUtils
import viz
import vizact
import AugRequester

gulbenkian = DesignUtils.load_gulbenkian_no_roof()

viz.MainWindow.fov(60)
viz.setMultiSample(4)
viz.clearcolor(viz.SKYBLUE)
viz.go()
CameraUtils.SetFTNCam(gulbenkian)

ar = AugRequester.AugRequester()

avatars = []

def askPath(startPos, endPos, model):
	global avatars
	ar.connect('134.102.148.214', 4950)
	avatars = AvatarUtils.CreateAvatars(ar.askForPath(startPos, endPos, model), 1.0, 4.59999275208, 'vcc_male.cfg')
	ar.close()
	
vizact.onkeydown('a', askPath, (14.9816, -0.299997, 49.7653), (50.0147, -0.300005, 50.3753), 'gulbenkian')
